#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char ** argv){

    void * memptr;
    int fd;

    if (argc != 2) {
        printf("USAGE: %s <filename>\n", argv[0]);
        exit(-1);
    }

    printf("%s: opening %s\n", argv[0], argv[1]);
    fd=open(argv[1], O_RDWR);

    if ((memptr = mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x1000)) == (void *) -1){
        printf("mmap failed (0x%p)\n", memptr);
        close(fd);
        exit(-1);
    }

    printf("mmap succeeded (0x%p)\n", memptr);

    printf("ivshmem contains: %s", (char *)memptr);

    printf("munmap \n");
    munmap(memptr, 256);

    printf("%s: closing %s\n", argv[0], argv[1]);
    close(fd);

    printf("%s: exiting.\n", argv[0]);
    return 0;
}
